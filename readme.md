ZeroTemplate is a very simple template engine that uses PHP's capability to process HTML files.

Installation
==

1. Install using `composer` or 
[download a release](https://gitlab.com/linkfast/exengine/zero-template/-/releases).

    ```
    ./composer.phar require linkfast/zero-template
    ````

Examples
===

Check some examples in the `examples` folder.

Usage
===

ZeroTemplate is ready to use since is installed (or included), just use the static function `exec()` to process a 
template file.

Basic Usage
====
```php
    <?php
    
    print ZeroTemplate::execString('<p><?=$placeHolder1?></p>', [
        "placeHolder1" => "Ok!"
    ]);
```

With Controller
====

In View
=====
```html
    <!-- file: myTemplate.phtml -->
    <html>
        <p>
            <?=$placeHolder1?>
        </p>    
    </html>
```   

In Controller
=====

```php
    <?php
    
    print ZeroTemplate::exec('myTemplate.phtml', [
        "placeHolder1" => "Ok!"
    ]);
```

The default template folder is `./Views/`.

Use with ExEngine
===

ZeroTemplate can be used easily with [ExEngine](https://gitlab.com/linkfast/exengine/exengine), just install it and then register as a singleton service in your
configuration instance.

```php
<?php    
        
    class MyConfiguration extends \ExEngine\BaseConfig {    
        // ...    
        function __construct($launcherFolderPath)
        {
            parent::__construct($launcherFolderPath);
            // ...
            ZeroTemplate::setDefaultLanguage("es");
            ZeroTemplate::setTemplatesFolder("Views");
            ZeroTemplate::setStaticParameter("framework", "EXENGINE.");
            $this->registerService(ZeroTemplate::class, true);
            // ...
        }
        // ...        
    }
```
    
Then when necessary inject in your controller's constructor.

```php
    <?php
    
    class MyController {        
        // ...
        private $zero;
        // ...
        public function __construct(ZeroTemplate $zero) {
            // ...
            $this->zero = $zero;
            // you can set some data here if you want, like the template file or some parameters.
            // ...            
        }
        // ...
        function index() {
            return $this->zero
                ->setTemplateFile('myTemplate.phtml')                
                ->setParameters(
                    ["placeHolder1" => "Ok!"]
                )
                ->render();
        }
    } 
```

Powered by ZeroTemplate
===

ZeroTemplate powers the lightest PHP RAD MVC framework [RadiancePHP](https://gitlab.com/linkfast/oss/radiance) for view 
rendering.

License
==

```
The MIT License (MIT)

Copyright (c) 2020 LinkFast S.A. (http://linkfast.io)
Copyright (c) 2020 Giancarlo A. Chiappe Aguilar (gchiappe@linkfast.io)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
```