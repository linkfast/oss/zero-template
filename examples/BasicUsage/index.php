<?php

include_once '../../src/zero-template.php';

ZeroTemplate::setTemplatesFolder(".");

$parameters = [
    "title" => "ZeroTemplate",
    "paragraph" => "Welcome to ZeroTemplate a simple yet powerful template engine for PHP.",
    "list" => [
        "Apple",
        "Orange",
        "Lemon",
        "Tomato"
    ]
];

echo ZeroTemplate::exec(
    "myview.phtml",
    $parameters
);

echo ZeroTemplate::execString(
    '<h2>Template as String rendering with <?=$title?></h2>
<p><?=$paragraph?>.</p>
<p><? print_r($list); ?></p>',
    $parameters
);