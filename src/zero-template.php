<?php

namespace {

    class ZeroTemplateException extends Exception {
        public function __construct($message = "", $code = 0, Throwable $previous = null)
        {
            parent::__construct($message, $code, $previous);
        }
    }

    class ZeroTemplate
    {
        static function setTemplatesFolder($location) {
            self::$templatesFolder = $location;
        }

        static function setStaticParameter($name, $value)
        {
            self::$staticParameters[$name] = $value;
        }

        static function setDefaultLanguage($languageCode) {
            self::$language = $languageCode;
        }

        /**
         * @param $templateFile
         * @param array $context
         * @param bool $fuseWithStatic
         * @return false|string
         * @throws ZeroTemplateException
         */
        static function exec($templateFile, $context = [], $fuseWithStatic = true)
        {
            return (new ZeroTemplate())
                ->setTemplateFile($templateFile)
                ->setParameters($context, $fuseWithStatic)
                ->render();
        }

        /**
         * @param $templateString
         * @param array $context
         * @param bool $fuseWithStatic
         * @return false|string
         * @throws ZeroTemplateException
         */
        static function execString($templateString, $context = [], $fuseWithStatic = true) {
            return (new ZeroTemplate())
                ->setTemplate($templateString)
                ->setParameters($context, $fuseWithStatic)
                ->render();
        }

        private $templateFile;
        private $templateStr;
        private $parameters = [];
        private $contextObject = null;
        private static $staticParameters = [];
        private static $templatesFolder = 'Views/';
        private static $language = 'en';

        /**
         * @param $templateFile string Path to the template file, recommended extensions are .phtml and .html.
         * @return $this
         * @throws ZeroTemplateException
         */
        function setTemplateFile($templateFile) {
            if (!is_null($this->templateStr))
                throw new ZeroTemplateException('ZeroTemplate: Requires to set the template as file or string but not both.');
            if (!file_exists($templateFile)) {
                if (!file_exists(self::$templatesFolder . '/' . $templateFile)) {
                    throw new ZeroTemplateException("ZeroTemplate: File $templateFile not found.");
                } else
                    $this->templateFile = self::$templatesFolder . '/' . $templateFile;
            } else
                $this->templateFile = $templateFile;
            return $this;
        }

        /**
         * @param $templateString string The complete to be rendered template contained in a string.
         * @throws ZeroTemplateException
         * @return $this
         */
        function setTemplate($templateString) {
            if (!is_null($this->templateFile))
                throw new ZeroTemplateException('ZeroTemplate: Requires to set the template as file or string but not both.');
            if (!is_string($templateString))
                throw new ZeroTemplateException('ZeroTemplate: $templateString is not a String.');
            $this->templateStr = $templateString;
            return $this;
        }

        /**
         * @param $parameters
         * @param bool $fuseWithStatic
         * @return $this
         * @throws ZeroTemplateException
         */
        function setParameters($parameters, $fuseWithStatic = true)
        {
            if (!is_null($this->contextObject)) {
                throw new ZeroTemplateException("ZeroTemplate requires that you use either 'setParameters' or " .
                    "'setContextObject' but not both.", 500);
            }
            $this->parameters = $parameters;
            if ($fuseWithStatic) {
                $this->parameters = array_merge($this->parameters, self::$staticParameters);
            }
            return $this;
        }

        /**
         * @return array
         */
        public function getParameters()
        {
            return $this->parameters;
        }

        /**
         * @return
         */
        public function getContextObject()
        {
            return $this->contextObject;
        }

        public function addParameter($parameter, $value) {
            $this->parameters[$parameter] = $value;
        }

        /**
         * @param $contextObject
         * @param bool $fuseWithStatic
         * @return $this
         * @throws ZeroTemplateException
         */
        function setContextObject($contextObject, $fuseWithStatic = true) {
            if (count($this->parameters) > 0) {
                throw new ZeroTemplateException("ZeroTemplate: Requires that you use either 'setParameters' or " .
                    "'setContextObject' but not both.", 500);
            }
            if ($fuseWithStatic) {
                $this->contextObject = $contextObject;
                $staticAsObject = (object) self::$staticParameters;
                foreach ($staticAsObject as $prop => $val) {
                    $this->contextObject->$prop = $val;
                }
            } else
                $this->contextObject = $contextObject;
            return $this;
        }

        /**
         * @param bool $autoPrint Set to true to automatically print the output, otherwise it will be returned.
         * @return false|string
         */
        function render($autoPrint = false)
        {
            if ($this->contextObject != null) {
                extract((array) $this->contextObject);
                extract(["context" => $this->contextObject]);
            } else {
                extract($this->parameters);
                extract(["context" => (object) $this->parameters]);
            }
            if (!is_null($this->templateFile)) {
                $preData = '?>'.preg_replace("/;*\s*\?>/", "; ?>",
                        str_replace('<?php=', '<?php echo ',
                            str_replace('<?', '<?php', file_get_contents($this->templateFile))
                        )
                    );
            } else {
                $preData = '?>'.preg_replace("/;*\s*\?>/", "; ?>",
                        str_replace('<?php=', '<?php echo ',
                            str_replace('<?', '<?php', $this->templateStr)
                        )
                    );
            }
            // Good ol' times from MVC-ExEngine (2016)
            // https://github.com/QOXCorp/exengine/blob/master/eefx/lib/mvc-exengine/mvcee_controller.php#L331
            ob_start();
            echo eval($preData);
            $template = ob_get_contents();
            ob_end_clean();
            // Now some new stuff...
            if (class_exists('\Mustache_Engine')) {
                $m = new Mustache_Engine(array('entity_flags' => ENT_QUOTES));
                if ($this->contextObject != null) {
                    $render = $m->render($template, $this->contextObject);
                } else {
                    $render = $m->render($template, $this->parameters);
                }
            } else $render = $template;
            if ($autoPrint) print $render;
            return $render;
        }
    }
}